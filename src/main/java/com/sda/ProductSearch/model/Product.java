package com.sda.ProductSearch.model;

public class Product {
	private int idProduct;
	private String productName;
	private String SKU;
	private Website website;
	private String price;
	private String prodURL;

	public Product(int idProduct, String productName, String SKU, Website website, String price, String prodURL) {
		super();
		this.idProduct = idProduct;
		this.productName = productName;
		this.SKU = SKU;
		this.website = website;
		this.price = price;
		this.prodURL = prodURL;
	}

	public Product() {

	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String SKU) {
		this.SKU = SKU;
	}

	public Website getWebsite() {
		return website;
	}

	public void setWebsite(Website website) {
		this.website = website;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String string) {
		this.price = string;
	}

	public String getProdURL() {
		return prodURL;
	}

	public void setProdURL(String prodURL) {
		this.prodURL = prodURL;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", productName=" + productName + ", SKU=" + SKU + ", idSiteConfig="
				+ ", price=" + price + ", siteURL=" + prodURL + "]";
	}

}
