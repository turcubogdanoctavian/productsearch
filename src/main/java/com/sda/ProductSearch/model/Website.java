package com.sda.ProductSearch.model;

import java.util.Set;

public class Website {
	private int id;
	private String siteAddress;
	private String searchLink;
	private String priceXPath;
	private String nameXPath;
	private String SKUXPath;
	private String urlXpath;

	private Set<Product> products;

	public Website(int id, String siteAddress, String searchLink, String priceXPath, String nameXPath, String sKUXPath,
			String urlXpath, Set<Product> products) {
		super();
		this.id = id;
		this.siteAddress = siteAddress;
		this.searchLink = searchLink;
		this.priceXPath = priceXPath;
		this.nameXPath = nameXPath;
		SKUXPath = sKUXPath;
		this.urlXpath = urlXpath;
		this.products = products;
	}

	public Website() {

	}

	public String getUrlXpath() {
		return urlXpath;
	}

	public void setUrlXpath(String urlXpath) {
		this.urlXpath = urlXpath;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {

		this.products = products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSearchLink() {
		return searchLink;
	}

	public void setSearchLink(String searchLink) {
		this.searchLink = searchLink;
	}

	public String getPriceXPath() {
		return priceXPath;
	}

	public void setPriceXPath(String priceXPath) {
		this.priceXPath = priceXPath;
	}

	public String getNameXPath() {
		return nameXPath;
	}

	public void setNameXPath(String nameXPath) {
		this.nameXPath = nameXPath;
	}

	public String getSKUXPath() {
		return SKUXPath;
	}

	public void setSKUXPath(String sKUXPath) {
		SKUXPath = sKUXPath;
	}

	@Override
	public String toString() {
		return "Website [id=" + id + ", siteAddress=" + siteAddress + ", searchLink=" + searchLink + ", priceXPath="
				+ priceXPath + ", nameXPath=" + nameXPath + ", SKUXPath=" + SKUXPath + ", urlXpath=" + urlXpath + "]";
	}

}
