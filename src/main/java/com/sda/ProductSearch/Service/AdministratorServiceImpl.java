package com.sda.ProductSearch.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.Dao.AdministratorDao;
import com.sda.ProductSearch.model.Administrator;

@Service
@Transactional
public class AdministratorServiceImpl implements AdministratorService {

	@Autowired
	private AdministratorDao administratorDao;

	@Override
	public List<Administrator> getAdministratorList() {
		return administratorDao.getAdministratorList();
	}

	@Override
	public void addAdministrator(Administrator administrator) {
		administratorDao.addAdministrator(administrator);

	}

	@Override
	public void deleteAdministrator(Administrator administrator) {
		administratorDao.deleteAdministrator(administrator);

	}

	@Override
	public void updateAdministrator(String id, String username, String password) {

		Administrator administrator = getAdministratorById(id);
		administrator.setUsername(username);
		administrator.setPassword(password);
	}

	@Override
	public void deleteAdministratorById(String id) {
		Administrator administrator = administratorDao.getAdministratorById(id);
		administratorDao.deleteAdministrator(administrator);

	}

	@Override
	public Administrator getAdministratorById(String id) {
		int administratorId = Integer.parseInt(id);

		for (Administrator administrator : administratorDao.getAdministratorList()) {
			if (administrator.getId() == administratorId) {
				return administrator;
			}
		}
		return null;
	}

	@Override
	public Administrator getAdministratorByUsername(String username) {
		return administratorDao.getAdministratorByUsername(username);
	}

}
