package com.sda.ProductSearch.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.AEADBadTagException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.DTO.ResultDTO;
import com.sda.ProductSearch.Dao.ProductDao;
import com.sda.ProductSearch.model.Product;
import com.sda.ProductSearch.model.Website;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	WebsiteService websiteService;
	@Autowired
	ProductDao productDao;

	// returns list of up-to-date ResultDTO objects
	@Override
	public List<ResultDTO> searchForProduct(String productName) throws Exception {
		List<ResultDTO> results = new ArrayList<>();

		// for each website, a list of products is made
		for (Website website : websiteService.getWebsiteList()) {
			results.addAll(buildProductList(website, productName));

			// for each product(resultDTO) from the each website( from results list)
			for (ResultDTO result : results) {

				// get product from site
				Product product = getProductFromSite(result.getWebsiteURL(), website);

				result.setProductName(product.getProductName());
				result.setProductPrice(product.getPrice());
				result.setSKU(product.getSKU());
				result.setWebsiteURL(product.getProdURL());

				// checks if product exists in DAO by website and by SKU(product unique code)
				if (productDao.getProductBySKUAndWebsite(product.getSKU(), product.getWebsite()) != null) {
					// if it is present in DAO, does price update
					Product existingProduct = productDao.getProductBySKUAndWebsite(product.getSKU(),
							product.getWebsite());

					// update price
					existingProduct.setPrice(product.getPrice());

					// sets id from database
					result.setIdProduct(existingProduct.getIdProduct());
					productDao.updateProduct(existingProduct);
				} else {

					Product existingProduct = productDao.getProductBySKUAndWebsite(product.getSKU(),
							product.getWebsite());
					result.setIdProduct(existingProduct.getIdProduct());

					// if ! present in DAO, adds product by website and by SKU
					productDao.addProduct(product);
				}
			}

		}
		return results;
	}

	// returns list of URLs
	private List<ResultDTO> buildProductList(Website website, String productName) throws Exception {
		URL websiteURL = buildWebsiteURL(website, productName);
		List<ResultDTO> results = new ArrayList<>();

		String page = readURL(websiteURL);

		// get URL regex from database
		Pattern URLRegex = Pattern.compile(website.getUrlXpath());

		// check if regex is present on the web page
		Matcher URLMatcher = URLRegex.matcher(page);

		// if URL is found on page, create ResultDTO that stores it
		while (URLMatcher.find()) {

			ResultDTO r = new ResultDTO();

			r.setWebsiteURL(URLMatcher.group(2));

			results.add(r);
		}

		return results;
	}

	// builds URL
	private URL buildWebsiteURL(Website website, String productName) throws Exception {
		URL websiteURL = new URL(
				website.getSiteAddress() + website.getSearchLink() + productName.replaceAll("\\s", ""));
		System.setProperty("http.agent", "Safari");
		return websiteURL;
	}

	// reads site content, saves site content in sb and returns it
	private String readURL(URL url) throws Exception {
		StringBuilder sb = new StringBuilder();

		BufferedReader in = new BufferedReader(new InputStreamReader(
				((HttpURLConnection) (url).openConnection()).getInputStream(), Charset.forName("UTF-8")));
		// new BufferedReader(new InputStreamReader(((HttpURLConnection) (new
		// URL(urlString)).openConnection()).getInputStream(),
		// Charset.forName("UTF-8")));

		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			sb.append(inputLine);
			sb.append(System.lineSeparator());
		}
		in.close();
		return sb.toString();
	}

	// returns product info from website
	private Product getProductFromSite(String productURL, Website website) throws MalformedURLException, Exception {
		String productPage = readURL(new URL(productURL));
		Product product = new Product();
		// get name, price, SKU regex from database
		Pattern nameRegex = Pattern.compile(website.getNameXPath());
		Pattern priceRegex = Pattern.compile(website.getPriceXPath());
		Pattern SKURegex = Pattern.compile(website.getSKUXPath());

		// check with regex if name, price and SKU are present on page
		Matcher priceMatcher = priceRegex.matcher(productPage);
		Matcher nameMatcher = nameRegex.matcher(productPage);
		Matcher SKUMatcher = SKURegex.matcher(productPage);

		while (nameMatcher.find() && priceMatcher.find() && SKUMatcher.find()) {

			product.setProductName(nameMatcher.group(2));
			product.setPrice(priceMatcher.group(3));
			product.setSKU(SKUMatcher.group(2));
			product.setWebsite(website);
			product.setProdURL(productURL);
		}

		return product;

	}

	public void addProduct(Product product) {
		productDao.addProduct(product);

	}

	public void updateProduct(Product product) {
		productDao.updateProduct(product);
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product> productList = productDao.getAllProducts();
		for (Product product : productList) {
			System.out.println(product);
		}
		return productList;
	}

	@Override
	public Product getProductById(int id) {

		return productDao.getProductById(id);
	}

	@SuppressWarnings("null")
	@Override
	public List<Product> getListOfProductsWithMatchingSKU(String SKU) {
		List<Product> productList = productDao.getAllProducts();
		List<Product> filteredList = new ArrayList<>();

		for (Product product : productList) {
			filteredList.add(productDao.getProductBySKUAndWebsite(SKU, product.getWebsite()));
		}

		return filteredList;
	}

}
