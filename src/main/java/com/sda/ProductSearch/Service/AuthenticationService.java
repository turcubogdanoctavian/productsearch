package com.sda.ProductSearch.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.model.Administrator;

@Service
@Transactional
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private AdministratorService administratorService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Administrator domainAdministrator = administratorService.getAdministratorByUsername(username);

		return new User(domainAdministrator.getUsername(), domainAdministrator.getPassword(), true, true, true, true,
				getAuthorities(domainAdministrator));
	}

	public Collection<? extends GrantedAuthority> getAuthorities(Administrator administrator) {
		System.out.println("AuthenticationService creating authorities for username: " + administrator.getUsername());
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		authorities.add(new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"));
		return authorities;
	}
}
