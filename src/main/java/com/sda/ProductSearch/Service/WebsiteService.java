package com.sda.ProductSearch.Service;

import java.net.URL;
import java.util.List;

import com.sda.ProductSearch.model.Website;

public interface WebsiteService {
	public List<Website> getWebsiteList();

	public void addWebsite(Website website);

	public void removeWebsite(Website website);

	public Website getSiteById(String id);

	public void removeSiteById(String id);

	public void updateSite(String id, String siteAddress, String searchLink, String priceXPath, String nameXPath);

	public void listAllSites();

}
