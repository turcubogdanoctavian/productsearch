package com.sda.ProductSearch.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.Dao.WebsiteDao;
import com.sda.ProductSearch.model.Website;

@Service
@Transactional

public class WebsiteServiceImpl implements WebsiteService {

	@Autowired
	WebsiteDao websiteDao;

	@Override
	public List<Website> getWebsiteList() {
		return websiteDao.getWebsiteList();
	}

	@Override
	public void addWebsite(Website website) {
		websiteDao.addWebsite(website);

	}

	@Override
	public void removeWebsite(Website website) {
		websiteDao.removeWebsite(website);

	}

	@Override
	public void removeSiteById(String id) {
		Website w = websiteDao.getWebsiteByid(id);
		websiteDao.removeWebsite(w);

	}

	@Override
	public void updateSite(String id, String siteAddress, String searchLink, String priceXPath, String nameXPath) {
		Website website = getSiteById(id);

		website.setSiteAddress(siteAddress);
		website.setSearchLink(searchLink);

		website.setPriceXPath(priceXPath);
		website.setNameXPath(nameXPath);

	}

	@Override
	public void listAllSites() {
		for (Website website : websiteDao.getWebsiteList()) {
			System.out.println(website);
		}

	}

	@Override
	public Website getSiteById(String id) {
		int websiteId = Integer.parseInt(id);
		for (Website website : websiteDao.getWebsiteList()) {
			if (website.getId() == websiteId) {
				return website;
			}
		}
		return null;

	}

}
