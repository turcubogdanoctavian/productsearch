package com.sda.ProductSearch.Service;

import java.util.List;

import com.sda.ProductSearch.model.Administrator;

public interface AdministratorService {

	public List<Administrator> getAdministratorList();

	public void addAdministrator(Administrator administrator);

	public void deleteAdministrator(Administrator administrator);

	public void updateAdministrator(String id, String username, String password);

	public void deleteAdministratorById(String id);

	public Administrator getAdministratorById(String id);

	public Administrator getAdministratorByUsername(String username);
}
