package com.sda.ProductSearch.Service;

import java.util.List;

import com.sda.ProductSearch.DTO.ResultDTO;
import com.sda.ProductSearch.model.Product;

public interface ProductService {

	public List<ResultDTO> searchForProduct(String productName) throws Exception;

	public List<Product> getAllProducts();

	public Product getProductById(int id);

	public List<Product> getListOfProductsWithMatchingSKU(String SKU);

}
