package com.sda.ProductSearch.Dao;

import java.io.Serializable;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.ProductSearch.model.Product;
import com.sda.ProductSearch.model.Website;

@Repository
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Product getProductBySKUAndWebsite(String SKU, Website website) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Product.class);
		criteria.createAlias("website", "website");
		criteria.add(Restrictions.eq("website.id", website.getId()));
		criteria.add(Restrictions.eq("SKU", SKU));
		return (Product) criteria.uniqueResult();

	}

	@Override
	public List<Product> getAllProducts() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Product> productList = session.createQuery("from Product").list();

		for (Product product : productList) {
			System.out.println(product);
		}

		transaction.commit();

		session.close();
		return productList;
	}

	@Override
	public void addProduct(Product product) {
		sessionFactory.getCurrentSession().save(product);

	}

	@Override
	public void updateProduct(Product product) {
		sessionFactory.getCurrentSession().update(product);

	}

	@Override
	public Product getProductById(int id) {

		Product product = (Product) sessionFactory.getCurrentSession().load(Product.class, id);

		System.out.println(product.getIdProduct() + " " + product.getProductName());
		return product;
	}

}
