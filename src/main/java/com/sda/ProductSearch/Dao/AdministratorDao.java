package com.sda.ProductSearch.Dao;

import java.util.List;
import java.util.Optional;

import com.sda.ProductSearch.model.Administrator;

public interface AdministratorDao {

	public void addAdministrator(Administrator administrator);

	public void deleteAdministrator(Administrator administrator);

	public void updateAdministrator(Administrator administrator);

	public void deleteAdministratorById(String id);

	public Administrator getAdministratorByUsername(String username);

	public Administrator getAdministratorById(String id);

	public List<Administrator> getAdministratorList();
}
