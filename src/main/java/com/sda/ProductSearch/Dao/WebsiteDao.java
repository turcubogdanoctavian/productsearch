package com.sda.ProductSearch.Dao;

import java.util.List;

import com.sda.ProductSearch.model.Website;

public interface WebsiteDao {

	public void addWebsite(Website website);

	public void removeWebsite(Website website);

	public void removeSiteById(String id);

	public void updateSite(Website website);

	public Website getWebsiteByid(String id);

	public List<Website> getWebsiteList();

}
