package com.sda.ProductSearch.Dao;

import java.util.List;
import java.util.Optional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.model.Administrator;
import com.sda.ProductSearch.model.Website;

@Repository
@Transactional
public class AdministratorDaoImpl implements AdministratorDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public Administrator getAdministratorByUsername(String username) {
		System.out.println("Getting administrator by username");
		Query query = sessionFactory.getCurrentSession().createQuery("from Administrator where username = :param")
				.setParameter("param", username);
		return (Administrator) query.uniqueResult();
	}

	@Override
	public void addAdministrator(Administrator administrator) {
		sessionFactory.getCurrentSession().save(administrator);

	}

	@Override
	public void deleteAdministrator(Administrator administrator) {
		sessionFactory.getCurrentSession().delete(administrator);

	}

	@Override
	public void updateAdministrator(Administrator administrator) {
		sessionFactory.getCurrentSession().update(administrator);

	}

	@Override
	public void deleteAdministratorById(String id) {
		Session session;
		Administrator administrator;
		session = sessionFactory.getCurrentSession();
		administrator = (Administrator) session.load(Administrator.class, id);
		session.delete(administrator);

		// This makes the pending delete to be done
		session.flush();

	}

	@Override
	public List<Administrator> getAdministratorList() {
		Session session = sessionFactory.openSession();
		Transaction transation = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Administrator> administratorList = session.createQuery("from Administrator").list();

		for (Administrator administrator : administratorList) {
			System.out.println(administrator);
		}

		transation.commit();
		return administratorList;
	}

	@Override
	public Administrator getAdministratorById(String id) {
		Session session;
		Administrator administrator;

		session = sessionFactory.getCurrentSession();
		administrator = (Administrator) session.load(Administrator.class, id);
		return administrator;
	}

}
