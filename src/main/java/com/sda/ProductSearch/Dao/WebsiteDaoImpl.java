package com.sda.ProductSearch.Dao;

import java.net.URL;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sda.ProductSearch.model.Website;

@Repository
public class WebsiteDaoImpl implements WebsiteDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Website getWebsiteByid(String id) {
		Session session;
		Website website;

		session = sessionFactory.getCurrentSession();
		website = (Website) session.load(Website.class, id);

		return website;

	}

	@Override
	public void removeSiteById(String id) {
		Session session;
		Website website;

		session = sessionFactory.getCurrentSession();
		website = (Website) session.load(Website.class, id);
		session.delete(website);

		// This makes the pending delete to be done
		session.flush();

	}

	@Override
	public void addWebsite(Website website) {

		sessionFactory.getCurrentSession().save(website);

	}

	@Override
	public void removeWebsite(Website website) {

		sessionFactory.getCurrentSession().delete(website);
	}

	@Override
	public void updateSite(Website website) {
		sessionFactory.getCurrentSession().update(website);

	}

	@Override
	public List<Website> getWebsiteList() {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<Website> websiteList = session.createQuery("from Website").list();

		for (Website website : websiteList) {
			System.out.println(website);
		}

		transaction.commit();

		session.close();
		return websiteList;
	}

}
