package com.sda.ProductSearch.Dao;

import java.util.List;

import com.sda.ProductSearch.model.Product;
import com.sda.ProductSearch.model.Website;

public interface ProductDao {

	public void addProduct(Product product);

	public void updateProduct(Product product);

	public Product getProductBySKUAndWebsite(String SKU, Website website);

	public List<Product> getAllProducts();

	public Product getProductById(int id);

}
