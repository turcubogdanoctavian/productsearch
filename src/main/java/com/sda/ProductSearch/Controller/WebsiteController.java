package com.sda.ProductSearch.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.ProductSearch.Service.ProductService;
import com.sda.ProductSearch.Service.WebsiteService;
import com.sda.ProductSearch.model.Product;

@Controller
@RequestMapping("WebsiteController")
public class WebsiteController {

	@Autowired
	WebsiteService websiteService;

	@Autowired
	ProductService productService;

	// http://localhost:8080/ProductSearch/WebsiteController/manageWebsiteList/
	@RequestMapping(value = "/manageWebsiteList", method = RequestMethod.GET)
	public String getWebsiteList(ModelMap model) {
		model.addAttribute("websiteList", websiteService.getWebsiteList());
		System.out.println("code entered from getWebsiteList");
		return "manageWebsite";
	}

	// http://localhost:8080/ProductSearch/WebsiteController/searchProduct/
	@RequestMapping(value = "/searchProduct", method = RequestMethod.GET)
	public String searchProduct(@RequestParam(value = "productName", required = false) String productName,
			ModelMap model) throws Exception {

		// make list with products that of which product name match the productName
		// inserted
		if (productName != null) {

			model.addAttribute("resultsList", productService.searchForProduct(productName));

		}
		return "searchProduct";

	}

	// http://localhost:8080/ProductSearch/WebsiteController/productPage?id=
	@RequestMapping(value = "/productPage", method = RequestMethod.GET)
	public String getProductPage(@RequestParam(value = "id", required = false) int id, Model model) throws Exception {

		Product product = new Product();
		product = productService.getProductById(id);
		System.out.println("Page with product:" + product.getProductName());

		model.addAttribute("productInstance", product);

		// create list of websites where the product is found

		List<Product> filteredProductsList = new ArrayList<Product>();
		filteredProductsList = productService.getListOfProductsWithMatchingSKU(product.getSKU());
		model.addAttribute("matchingSKUList", filteredProductsList);

		return "productPage";

	}

}
