package com.sda.ProductSearch.DTO;

import java.net.URL;

public class ResultDTO {
	private int idProduct;
	private String websiteURL;
	private String productName;
	private String productPrice;
	private String SKU;

	public String getWebsiteURL() {
		return websiteURL;
	}

	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	@Override
	public String toString() {
		return "ResultDTO [idProduct=" + idProduct + ", websiteURL=" + websiteURL + ", productName=" + productName
				+ ", productPrice=" + productPrice + ", SKU=" + SKU + "]";
	}

}
