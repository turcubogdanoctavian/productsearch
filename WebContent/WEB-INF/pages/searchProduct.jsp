
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.sda.ProductSearch.Controller.WebsiteController"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Product Search Page</title>
</head>
<body>
	Sneaker search


	<form method="GET"
		action="http://localhost:8080/ProductSearch/WebsiteController/searchProduct/">
		<input type="text" align="middle" name="productName"
			placeholder="Product"> <input type="submit" name="Search"
			value="Search">
	</form>

	<form
		action="http://localhost:8080/ProductSearch/WebsiteController/manageWebsiteList/">
		<button name="adminButton">Admin</button>
	</form>

	<!-- for each product found that matches the name, make a product page 
	
	-->
	<table>
		<tr>
			<td><c:forEach items="${resultsList}" var="product">

					<tr>
						<td><a
							href="http://localhost:8080/ProductSearch/WebsiteController/productPage?id=${product.getIdProduct()}"><c:out
									value="${product.getProductName()}"></c:out></a>
					</tr>
				</c:forEach></td>
		</tr>
	</table>

</body>
</html>