
<%@page import="com.sda.ProductSearch.Service.WebsiteServiceImpl"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Website Manage Page</title>

</head>
<body>



	<table>
		<c:forEach items="${websiteList}" var="iterator">
			<tr>
				<td><c:out value="${iterator}"></c:out></td>
				<td><form method="GET"
						action="http://localhost:8080/ProductSearch/Controller/getWebsiteList/UpdateWebsite">
						<input type="submit" value="Update"> <input type="hidden"
							name="websiteId" value="${iterator.getId()}">
					</form></td>
				<td><form method="GET"
						action="http://localhost:8080/ProductSearch/Controller/getWebsiteList/DeleteCustomer">
						<input type="submit" value="Delete"> <input type="hidden"
							name="websiteId" value="${iterator.getId()}">
					</form></td>
			</tr>
		</c:forEach>
	</table>

	<form method="POST"
		action="http://localhost:8080/ProductSearch/Controller/getWebsiteList/AddCustomer">
		Website Address: <input type="text" name="websiteAddress">
		Search Link: <input type="text" name="searchLink"> Price
		Xpath:<input type="text" name="priceXPath"> Name Xpath:<input
			type="text" name="nameXPath"> <input type="submit"
			value="Add Website">
	</form>

	<form
		action="http://localhost:8080/ProductSearch/WebsiteController/searchProduct/">
		<button name="manageWebsiteBackButton">Back</button>
	</form>

	<form:form action="${pageContext.request.contextPath}/logout"
		method="POST">
		<input type="submit" value="Logout" />
	</form:form>

</body>
</html>