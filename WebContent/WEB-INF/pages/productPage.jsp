<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.sda.ProductSearch.model.Product"%>
<%@ page import="com.sda.ProductSearch.DTO.ResultDTO"%>
<%@ page import="com.sda.ProductSearch.model.Website"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Product Page</title>
</head>
<body>



	<table>
		<tr>
			<td><a><c:out value="${productInstance.getProductName()}"></c:out></a></td>
		</tr>

		<tr>
			<td><c:forEach items="${matchingSKUList}" var="product">
					<%!Website website = new Website();%>



					<tr>
						<td><a href="${product.getProdURL()}"><c:out
									value="${website.getSiteAddress()}"></c:out></a>
					</tr>
				</c:forEach></td>
		</tr>
	</table>
	<form
		action="http://localhost:8080/ProductSearch/WebsiteController/searchProduct/">
		<button name="productPageBackButton">Back</button>
	</form>



</body>
</html>